import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';

abstract class Repository {
  DioError makeDioError(HttpResponse<dynamic> httpResponse) {
    return DioError(
      error: httpResponse.response.statusMessage,
      response: httpResponse.response,
      requestOptions: httpResponse.response.requestOptions,
      type: DioErrorType.response,
    );
  }
}