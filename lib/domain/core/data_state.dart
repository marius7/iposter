import 'package:dio/dio.dart';

abstract class DataState<T> {
  final T? data;
  final Exception? error;
  final DioError? dioerror;
  const DataState({this.data, this.error,this.dioerror});
}

class DataSuccess<T> extends DataState<T> {
  const DataSuccess(T data) : super(data: data);
}

class DataFailed<T> extends DataState<T> {
  const DataFailed(Exception exception) : super(error: exception);
}

class DataFailDio<T> extends DataState<T> {
  const DataFailDio(DioError error) : super(dioerror: error);
}
