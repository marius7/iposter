import 'package:djamo_todo/domain/core/data_state.dart';
import 'package:djamo_todo/domain/core/repository.dart';
import 'package:djamo_todo/domain/todo/todo.dart';

abstract class ITodoRepository {
  Future<DataState<List<Todo>>> all();

  Future<DataState<Todo>> create(Todo todo);

  Future<DataState<Todo>> get(int id);

  Future<DataState<Todo>> update(Todo todo,int id);

  Future<DataState<void>> delete(int id);
}
