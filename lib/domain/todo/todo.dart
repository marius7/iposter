import 'package:json_annotation/json_annotation.dart';

part 'todo.g.dart';

@JsonSerializable()
class Todo {
  final int? userId;
  final int? id;
  final String title;
  final bool completed;

  Todo({this.userId,  this.id, required this.title, required this.completed});

  Map<String, dynamic> toJson() => _$TodoToJson(this);

  factory Todo.fromJson(Map<String, dynamic> json) => _$TodoFromJson(json);
}