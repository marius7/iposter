import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';


class Ui {

  static ErrorSnackBar({String title = 'Error', String? message}) {
    Get.log("[$title] $message", isError: true);

    Get.showSnackbar(GetBar(
      messageText: Text(
        message!,
        style: const TextStyle(
            fontSize: 16.0, color: Colors.red, fontWeight: FontWeight.bold),
      ),
      snackPosition: SnackPosition.BOTTOM,
      margin: const EdgeInsets.all(20),
      backgroundColor: Colors.white,
      icon: Icon(Icons.remove_circle_outline, size: 32, color: Colors.red),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      borderRadius: 8,
      duration: const Duration(seconds: 5),
    ));
  }

  static GetBar defaultSnackBar(
      {String title = 'Alert', required String message}) {
    Get.log("[$title] $message", isError: false);
    return GetBar(
      messageText: Text(
        message,
        style: const TextStyle(
            fontSize: 16.0, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      snackPosition: SnackPosition.BOTTOM,
      margin: const EdgeInsets.all(20),
      backgroundColor: Colors.black54,
      borderColor: Get.theme.focusColor.withOpacity(0.1),
      icon: Icon(Icons.warning_amber_rounded, size: 32, color: Colors.white),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      borderRadius: 8,
      duration: const Duration(seconds: 5),
    );
  }

  static GetBar notificationSnackBar(
      {String title = 'Notification', required String message}) {
    Get.log("[$title] $message", isError: false);
    return GetBar(
      titleText: Text(title),
      messageText: Text(message),
      snackPosition: SnackPosition.TOP,
      margin: const EdgeInsets.all(20),
      backgroundColor: Get.theme.primaryColor,
      borderColor: Get.theme.focusColor.withOpacity(0.1),
      icon:
          Icon(Icons.notifications_none, size: 32, color: Get.theme.hintColor),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      borderRadius: 8,
      duration: const Duration(seconds: 5),
    );
  }
}
