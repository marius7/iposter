import 'package:djamo_todo/application/todo/todo_controller.dart';
import 'package:djamo_todo/presentation/screen/home/widgets/list_todo.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeWidget extends GetView<TodoController> {
  HomeWidget({Key? key}) : super(key: key);
  final ScrollController _scrollController = ScrollController();

  Future<void> loadData(bool reload) async {
    controller.getListTodo();
  }

  @override
  Widget build(BuildContext context) {
    controller.formKey = GlobalKey<FormState>();
    loadData(false);
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          controller: _scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          slivers: [
            SliverPersistentHeader(
              pinned: true,
              delegate: SliverDelegate(
                  child: Center(
                      child: Container(
                height: 50,
                width: Get.width,
                color: Theme.of(context).backgroundColor,
                child: InkWell(
                  onTap: () => {},
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).cardColor,
                    ),
                    child: Row(children: [
                      Expanded(
                        child: Form(
                          key: controller.formKey,
                          child: TextFormField(
                            controller: controller.textEditingControllerTask,
                            validator: (value) {},
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Enter a search term',
                            ),
                          ),
                        ),
                      ),
                      IconButton(
                          onPressed: () {
                            controller.addTodo();
                          },
                          icon: Icon(Icons.add))
                    ]),
                  ),
                ),
              ))),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Container(child: ListTodo()),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SliverDelegate extends SliverPersistentHeaderDelegate {
  Widget child;

  SliverDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => 50;

  @override
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverDelegate oldDelegate) {
    return oldDelegate.maxExtent != 50 ||
        oldDelegate.minExtent != 50 ||
        child != oldDelegate.child;
  }
}
