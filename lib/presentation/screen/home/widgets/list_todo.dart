import 'package:flutter/material.dart';
import 'package:djamo_todo/application/todo/todo_controller.dart';
import 'package:get/get.dart';

class ListTodo extends GetView<TodoController> {
  @override
  Widget build(BuildContext context) {
    controller.formKeyEdit = GlobalKey();
    var parentContext = context;
    return SizedBox(
        height: Get.height * .70,
        child: Obx(() {
          return ListView.builder(
              itemCount: controller.todos.value.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    showBottomSheetTodoDetail(context, controller, index);
                  },
                  child: ListTile(
                      leading: Text(controller.todos.value[index].title),
                      trailing: PopupMenuButton(
                          itemBuilder: (context) {
                            return [
                              PopupMenuItem(
                                child: Text('Modifier'),
                                onTap: () {
                                  Future.delayed(const Duration(seconds: 0),
                                      () => showBottomSheet(controller, index));
                                },
                              ),
                              PopupMenuItem(
                                child: Text('Supprimer'),
                                onTap: () {
                                  controller.deleteTodo(
                                      controller.todos.value[index], index);
                                },
                              )
                            ];
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: Icon(Icons.menu),
                          ))),
                );
              });
        }));
  }
}

showBottomSheet(controller, index) {
  controller.textEditingControllerEditTask.text =
      controller.todos.value[index].title;
  showDialog(
    context: Get.context!,
    builder: (context) {
      return AlertDialog(
        title: const Text('Modifier la tache'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(children: [
                Expanded(
                  child: Form(
                    key: controller.formKeyEdit,
                    child: TextFormField(
                      controller: controller.textEditingControllerEditTask,
                      validator: (value) {},
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter a search term',
                      ),
                    ),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      controller.updateTodo(
                          controller.todos.value[index], index);
                    },
                    icon: Icon(Icons.save))
              ])
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('ANNULER'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

showBottomSheetTodoDetail(context, controller, index) {
  controller.textEditingControllerEditTask.text =
      controller.todos.value[index].title;
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: const Text('Detail de la tache'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text("Titre : ${controller.todos.value[index].title}"),
              Text(
                  "Status : ${controller.todos.value[index].completed ? 'Terminé' : 'En cour'}"),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('ANNULER'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
