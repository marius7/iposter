import 'package:djamo_todo/presentation/screen/home/home_widget.dart';
import 'package:get/get.dart';

import 'app_route.dart';

class AppPages {
  static const String initial = AppRoutes.home;
  static final List<GetPage> routes = [
    GetPage(name: AppRoutes.home, page: () => HomeWidget()),
  ];
}
