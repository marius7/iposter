import 'package:djamo_todo/application/core/constant.dart';
import 'package:djamo_todo/domain/core/data_state.dart';
import 'package:djamo_todo/domain/todo/todo.dart';
import 'package:djamo_todo/infrastructure/todo/todo_repository.dart';
import 'package:djamo_todo/presentation/common/widgets/ui.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TodoController extends GetxController implements GetxService {
  TextEditingController textEditingControllerTask = TextEditingController();
  TextEditingController textEditingControllerEditTask = TextEditingController();
  late GlobalKey<FormState> formKey;
  late GlobalKey<FormState> formKeyEdit;
  final TodoRepository todoRepository;
  RxList<dynamic> todos = [].obs;

  TodoController(this.todoRepository);

  getListTodo() async {
    final result = await todoRepository.all();

    if (result is DataSuccess) {
      todos.value = result.data!;
      update();
    }
    if (result is DataFailed) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: kErrorOccuredMessage));
    }
  }

  addTodo() async {
    if (formKey.currentState!.validate()) {
      int userId = todos.isNotEmpty ? todos.first.userId! : 1;

      Todo todo = Todo(
          title: textEditingControllerTask.text,
          completed: false,
          userId: userId);
      final result = await todoRepository.create(todo);

      if (result is DataSuccess) {
        Get.log(todos.value.length.toString());
        todos.value = [result.data!, ...todos.value];
        textEditingControllerTask.text = "";
        update();
      }
      if (result is DataFailDio) {
        Get.log(result.dioerror!.message);
        //Get.showSnackbar(Ui.ErrorSnackBar(message: kErrorOccuredMessage));
      }
    }
  }

  updateTodo(Todo todo, index) async {
    if (formKeyEdit.currentState!.validate()) {
      Todo todoForUpdate = Todo(
          title: textEditingControllerEditTask.text,
          completed: todo.completed,
          id: todo.id,
          userId: todo.userId);

      final result = await todoRepository.update(todoForUpdate, todo.id!);

      if (result is DataSuccess) {
        Get.log(todos.value.length.toString());
        todos.value[index] = todoForUpdate;
        todos.value = [...todos.value];
        textEditingControllerEditTask.text = "";
        Get.back();
        update();
      }
      if (result is DataFailDio) {
        Get.log(result.dioerror!.message);
        //Get.showSnackbar(Ui.ErrorSnackBar(message: kErrorOccuredMessage));
      }
    }
  }

  deleteTodo(Todo todo, index) async {
    Todo todoForUpdate = Todo(
        title: textEditingControllerEditTask.text,
        completed: todo.completed,
        id: todo.id,
        userId: todo.userId);

    final result = await todoRepository.delete(todo.id!);

    if (result is DataSuccess) {
      todos.value.removeAt(index);
      todos.value = [...todos.value];
      Get.back();
      update();
    }
    if (result is DataFailDio) {
      Get.log(result.dioerror!.message);
      //Get.showSnackbar(Ui.ErrorSnackBar(message: kErrorOccuredMessage));
    }
  }

  showDialog(){
    Get.defaultDialog(
        title: "GeeksforGeeks",
        middleText: "Hello world!",
        backgroundColor: Colors.green,
        titleStyle: TextStyle(color: Colors.white),
        middleTextStyle: TextStyle(color: Colors.white),
        textConfirm: "Confirm",
        textCancel: "Cancel",
        cancelTextColor: Colors.white,
        confirmTextColor: Colors.white,
        buttonColor: Colors.red,
        barrierDismissible: false,
        radius: 50,
        content: Column(
          children: [
            Container(child:Text("Hello 1")),
            Container(child:Text("Hello 2")),
            Container(child:Text("Hello 3")),
          ],
        )
    );
  }
}
