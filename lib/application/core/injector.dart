import 'package:djamo_todo/application/todo/todo_controller.dart';
import 'package:djamo_todo/domain/todo/i_note_repository.dart';
import 'package:djamo_todo/infrastructure/datasources/dio_http_client.dart';
import 'package:djamo_todo/infrastructure/datasources/todo_api_service.dart';
import 'package:djamo_todo/infrastructure/todo/todo_repository.dart';
import 'package:get/get.dart';

class Injector extends Bindings {
  @override
  void dependencies() {
    Get.log("Binding");
    Get.lazyPut<DioHttpClient>(
      () => DioHttpClient(),
    );

    Get.lazyPut<TodoApiService>(
      () => TodoApiService(Get.find<DioHttpClient>().dio),
    );

    Get.lazyPut<TodoRepository>(
      () => TodoRepository(todoApiService: Get.find<TodoApiService>()),
    );

    Get.lazyPut<TodoController>(
      () => TodoController(Get.find<TodoRepository>()),
    );
  }
}
