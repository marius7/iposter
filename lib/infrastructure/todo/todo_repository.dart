import 'dart:io';

import 'package:dio/dio.dart';
import 'package:djamo_todo/domain/core/data_state.dart';
import 'package:djamo_todo/domain/core/repository.dart';
import 'package:djamo_todo/domain/todo/i_note_repository.dart';
import 'package:djamo_todo/domain/todo/todo.dart';
import 'package:djamo_todo/infrastructure/datasources/todo_api_service.dart';

class TodoRepository extends Repository implements ITodoRepository {
  final TodoApiService todoApiService;

  TodoRepository({required this.todoApiService});

  @override
  Future<DataState<List<Todo>>> all() async {
    try {
      final httpResponse = await todoApiService.all();

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }
      return DataFailDio(
        makeDioError(httpResponse),
      );
    } on DioError catch (e) {
      return DataFailDio(e);
    }
  }

  @override
  Future<DataState<Todo>> create(Todo todo) async {
    try {
      final httpResponse = await todoApiService.create(todo);

      return DataSuccess(httpResponse.data);
    } on DioError catch (e) {
      return DataFailDio(e);
    }
  }

  @override
  Future<DataState<void>> delete(int id) async {
    try {
      final httpResponse = await todoApiService.delete(id);

      return DataSuccess(httpResponse.data);
    } on DioError catch (e) {
      return DataFailDio(e);
    }
  }

  @override
  Future<DataState<Todo>> update(Todo todo, int id) async {
    try {
      final httpResponse = await todoApiService.update(todo, id);

      return DataSuccess(httpResponse.data);
    } on DioError catch (e) {
      return DataFailDio(e);
    }
  }

  @override
  Future<DataState<Todo>> get(int id) async {
    try {
      final httpResponse = await todoApiService.get(id);

      return DataSuccess(httpResponse.data);
    } on DioError catch (e) {
      return DataFailDio(e);
    }
  }
}
