import 'package:dio/dio.dart';
import 'package:djamo_todo/domain/todo/todo.dart';
import 'package:djamo_todo/infrastructure/core/constant.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'todo_api_service.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class TodoApiService {
  factory TodoApiService(Dio dio, {String baseUrl}) = _TodoApiService;

  @GET('/todos')
  Future<HttpResponse<List<Todo>>> all();

  @POST('/todos')
  Future<HttpResponse<Todo>> create(@Body() Todo todo);

  @GET("/todos/{id}")
  Future<HttpResponse<Todo>> get(@Path("id") int id);

  @PUT('/todos/{id}')
  Future<HttpResponse<Todo>> update(@Body() Todo todo, @Path() int id);

  @DELETE('/todos/{id}')
  Future<HttpResponse<void>> delete(@Path() int id);
}
