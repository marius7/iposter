import 'package:dio/dio.dart';
import 'package:get/get.dart';

class DioHttpClient extends GetxService {
  late final Dio _HttpClient;

  DioHttpClient() {
    _HttpClient = Dio();
    _HttpClient.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) {
      print(options.headers.toString());
      print(options.uri.toString());
      print(options.headers.toString());
      return handler.next(options); //continue
    }, onResponse: (response, handler) {
      print("responseInterceptor" + response.toString());
      return handler.next(response); // continue
    }, onError: (DioError e, handler) {
      print("errorInterceptor ${e.message}");
      print("errorInterceptor ${e.response}");
      return handler.next(e); //continue
    }));
  }

  requestInterceptor(RequestOptions options) async {
    print(options.headers.toString());
    print(options.uri.toString());
    print(options.headers.toString());
    return options;
  }

  Future responseInterceptor(response) async {
    print("responseInterceptor" + response.toString());
    return response;
  }

  errorInterceptor(DioError dioError) async {
    print("errorInterceptor ${dioError.message}");
    print("errorInterceptor ${dioError.response}");
  }

  Dio get dio => _HttpClient;
}
